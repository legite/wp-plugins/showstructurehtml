Name: showstructurehtml
Type: plugin Wordpress
Author: Pascal TOLEOO
Date: 2024.04.10

Ce plugin affiche  la description de certaines balises HTLM à l'intérieur de la balise.
Cela aide à savoir où se situe ces balises dans la page HTML
