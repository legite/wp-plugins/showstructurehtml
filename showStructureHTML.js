//var element = document.createElement(tagName[, options]);
//https://developer.mozilla.org/fr/docs/Web/API/document/createElement


addEventListener("load", (event) => {
    attachText(document.getElementById('body'));
    showStructure_showElements('header');
    showStructure_showElements('main');
    showStructure_showElements('div');
    showStructure_showElements('footer');
    var a = 1;
});
/*
function showStructure_onload() {
    attachText(document.getElementById('body'));
    showStructure_showElements('header');
    showStructure_showElements('main');
    showStructure_showElements('div');
    showStructure_showElements('footer');
    var a = 1;
}
*/
function showStructure_showElements(eltGroupe) {
    if (!eltGroupe) { return; }
    var allElt = document.getElementsByTagName(eltGroupe);
    var num = allElt.length;
    //alert("There are " + num + " paragraph in this document");
    for (i = 0; i < allElt.length; i++) {
        if (allElt === null) { continue; }
        attachText(allElt[i]);
    }
}

function attachText(eltId) {
    if (eltId === null) {
        return;
    }

    // Exclure les showStructure
    if (eltId.className == "showStructure") { return; }

    //var nodeName=eltId.nodeName;
    var outerHTML = eltId.outerHTML;

    // crée un nouvel élément div
    //var newDiv = document.createElement("div");
    //newDiv.classList.add("showStructure");

    var entete = outerHTML.split(">");
    if (!entete) { return; }
    entete = entete[0] + ">";

    // Sortie sur exclusion
    switch (entete){
        case '<div class="exceptionTitre">':
        case '<div class="inspectVarsOrigine">':
        case '<div class="inspectVars">':
        case '<div class="inspectVarsArray">':
        case '<div class="exceptionData">':
            return;
        break;
    }
    entete = entete.replace('<', '&lt;');
    entete = entete.replace('>', '&gt;');
    //var newText = document.createTextNode(entete);
    //newDiv.appendChild(newText);
    //document.body.insertBefore(newDiv, eltId);
    entete = '<span class="showStructure">' + entete + '</span>';
    eltId.insertAdjacentHTML("afterbegin", entete);
}
