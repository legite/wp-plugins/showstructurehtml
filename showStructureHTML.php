<?php
/**
 * @package showStructureHTML
 * @version 0.0.1
 */
/*
Plugin Name: show Structure HTML
Plugin URI: 
Description: Affiche l'entete HTML de certaines balises HTML
Author: legite.org
Version: 0.0.1
Author URI: https://legite.org
*/
define ('PLUGIN_SHOW_STRUCTUREHTML_ROOT', plugin_dir_path( __FILE__ )); // finit par DIRECTORY_SEPARATOR
define ('PLUGIN_SHOW_STRUCTUREHTML_URI' , plugin_dir_url ( __FILE__ )); // finit par DIRECTORY_SEPARATOR

//echo "PLUGIN_SHOW_STRUCTUREHTML_ROOT:".PLUGIN_SHOW_STRUCTUREHTML_ROOT.'<br>';
//echo "PLUGIN_SHOW_STRUCTUREHTML_URI:".PLUGIN_SHOW_STRUCTUREHTML_URI.'<br>';
function showStructureHTML_css_load_js_css(){
    wp_enqueue_style( 'showStructureHTML_css', PLUGIN_SHOW_STRUCTUREHTML_URI . 'showStructureHTML.css' );
    wp_enqueue_script( 'showStructureHTML_js', PLUGIN_SHOW_STRUCTUREHTML_URI . 'showStructureHTML.js', array(), '1.0.0', false );
}


add_action( 'wp_enqueue_scripts', 'showStructureHTML_css_load_js_css' );
//add_action( 'wp_loaded', 'showStructureHTML_');
function showStructureHTML_go() {
    if ( !is_admin() ) { 

    }
}